<?php

namespace Drupal\lazy_service_example;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\lazy_service_example\EventSubscriber\myLazyInterface;

/**
 * Lazy service event subscriber.
 */
class myLazy implements myLazyInterface {

  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  public function doSomething() {
    // @todo Place code here.
    $this->messenger->addMessage('I\'m Lazy');
  }

}
