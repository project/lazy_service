<?php

namespace Drupal\lazy_service;

use Drupal\Component\ProxyBuilder\ProxyBuilder;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

class LazyServiceServiceProvider extends ServiceProviderBase {
  const PROXY_CLASS_DIRECTORY = 'sites/default/files/php/ProxyClass';
  const PROXY_CLASS_ROOT_NAMESPACE = 'Drupal\ProxyClass';

  public function alter(ContainerBuilder $container) {
    $definitions = $container->getDefinitions();

    $namespaces = $container->getParameter('container.namespaces');
    if (!isset($namespaces[self::PROXY_CLASS_ROOT_NAMESPACE])) {
      $namespaces[self::PROXY_CLASS_ROOT_NAMESPACE] = self::PROXY_CLASS_DIRECTORY;
    }
    $container->setParameter('container.namespaces', $namespaces);

    foreach ($definitions as $definition) {
      $arguments = $definition->getArguments();
      if (empty($arguments)) {
        continue;
      }

      foreach ($arguments as $key => $argument) {
        if (is_array($argument)) {
          // TODO: Make all this recursive.
          continue;
        }
        $argumentName = (string) $argument;
        if ( empty($argumentName) || (!str_starts_with($argumentName, 'lazy.'))) {
          continue;
        }

        if (isset($definitions[$argumentName])) {
          continue;
        }

        $originalServiceName = substr($argumentName, 5);
        if (!isset($definitions[$originalServiceName])) {
          throw new ServiceNotFoundException($originalServiceName);
        }

        $definition->setArgument($key, new Reference($originalServiceName));
        $originalServiceDefinition = $definitions[$originalServiceName];
        $this->proxyBuilder = new ProxyBuilder();
        // Create Proxy
        // TODO: Check class has not been already generated.
        $proxy_class_name = $this->generate($originalServiceDefinition->getClass());

        if (NULL === $proxy_class_name) {
          // TODO
          throw new \Exception('Something goes wrong generating proxy class.');
        }

        $argumentDefinition = $container->getDefinition($originalServiceName);
        $argumentDefinition->setLazy(TRUE);
        $container->setDefinition($originalServiceName, $argumentDefinition);

        $lazyDefinition = clone($argumentDefinition);
        $lazyDefinition->setLazy(FALSE);
        $lazyDefinition->setPublic(TRUE);
        $new_service_id = 'drupal.proxy_original_service.' . $originalServiceName;
        $container->setDefinition($new_service_id, $lazyDefinition);

        $container->register($originalServiceName, $proxy_class_name)
          ->setArguments([new Reference('service_container'), $new_service_id]);
      }

    }
  }

  private function generate($class_name) {
    $match = [];
    preg_match('/([a-zA-Z0-9_]+\\\\[a-zA-Z0-9_]+)\\\\(.+)/', $class_name, $match);

    if ($match) {
      $rest_fqcn = $match[2];

      $proxy_filename = DRUPAL_ROOT . '/' . self::PROXY_CLASS_DIRECTORY . '/' . str_replace('\\', '/', $class_name) . '.php';
      $proxy_class_name = self::PROXY_CLASS_ROOT_NAMESPACE . '\\' . $class_name;
      if (file_exists($proxy_filename)) {
        return $proxy_class_name;
      }

      $proxy_class_string = $this->proxyBuilder->build($class_name);

      $file_string = <<<EOF
<?php
// phpcs:ignoreFile

/**
 * This file was generated via php \Drupal\lazy_service\LazyServiceProvider '$class_name'.
 */
{{ proxy_class_string }}
EOF;
      $original_proxy_namespace = $this->proxyBuilder::buildProxyNamespace($class_name);
      $proxy_namespace = substr($proxy_class_name, 0, -1 * (strlen($rest_fqcn) + 1));
      $file_string = str_replace(['{{ proxy_class_name }}', '{{ proxy_class_string }}', $original_proxy_namespace], [$proxy_class_name, $proxy_class_string, $proxy_namespace], $file_string);

      mkdir(dirname($proxy_filename), 0775, TRUE);
      file_put_contents($proxy_filename, $file_string);

      return $proxy_class_name;
    }

    return NULL;
  }
}
